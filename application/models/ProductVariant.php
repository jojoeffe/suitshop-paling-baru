<?php 

	class ProductVariant extends ActiveRecord\Model{
		
		static $table_name = 'product_variants';

		static $belongs_to = 
			array(array('product'))
		;

		//validations:
		static $validates_presence_of = array(
			//array('name', 'message' => 'nama tidak boleh kosong!'),
			array('price', 'message' => 'harga tidak boleh kosong!'),
			);
		static $validates_numericality_of = array(
			array('price','greater_than' => 0, 'message' => 'harga tidakboleh negatif atau kosong'),
			);

		static $before_destroy = array(
			'delete_uploaded_image'
			);
		/*
		static $per_page = 6;

		public function page($page_number){
			return Product::find('all', array('offset'=>($page_number -1)*self::$per_page, 'limit'=>self::$per_page));
		}
		*/
		public function image_url(){
			if($this->image_url){
				return base_url('uploads/product/'.$this->id.'/'.$this->image_url);
			}else{
				return false;
			}
		}

		public function upload_image()
		{
			$CI =& get_instance();
			$CI->load->library('upload');
			if(isset($_FILES['product_image']['size']) && $_FILES['product_image']['size']>0){
				$target_dir = getcwd().'/uploads/product/'.$this->id;
				echo $target_dir;
				if(!file_exists($target_dir)){
					mkdir($target_dir);
				}

				$config['upload_path'] = $target_dir;
				$config['allowed_types'] = 'gif|png|jpg|bmp';
				$config['max_size'] = '1000000';
				$config['max_width'] = '2048';
				$config['max_height'] = '1536';
				$CI->upload->initialize($config);

				$this->delete_uploaded_image();

				if($CI->upload->do_upload('product_image')){
					$upload_data = $CI->upload->data();
					$this->image_url = $upload_data['file_name'];
					return true;
				}
				else{
					echo $this->upload->display_errors();
					return false;
				}
			}
		}

		public function delete_uploaded_image(){
			$full_path = getcwd().'/uploads/product/'.$this->id.'/'.$this->image_url;
			if(isset($this->image_url) && file_exists($full_path)){
				unlink($full_path);
			}
		}

	}