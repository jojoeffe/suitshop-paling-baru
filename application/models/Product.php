<?php 

	class Product extends ActiveRecord\Model{
		static $belongs_to = 
			array(array('category'))
		;

		static $has_many = array(
			array('product_variants')
		);

		static $per_page = 6;

		public function page($page_number){
			return Product::find('all', array('offset'=>($page_number -1)*self::$per_page, 'limit'=>self::$per_page));
		}

	}
 ?>