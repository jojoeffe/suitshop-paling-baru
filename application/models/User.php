<?php 

	class User extends ActiveRecord\Model{
		var $password;
		var $password_confirmation;

		public function set_password($password, $password_confirmation){
			$this->password = $password;
			$this->password_confirmation = $password_confirmation;
		}

		public function validate(){
			if($this->password!=$this->password_confirmation){
				$this->error->add('password', 'password and password confirmation dont match');
			}else{
				echo $this->password;
				echo $this->password_confirmation;
				$this->encrypted_password = md5($this->password);
				//$this->password = '';
				echo $this->encrypted_password;
			}
		}

		static $table_name = 'users';
		/*static $has_many = array(
			array('user_addresses')
		);*/

		static $validates_presence_of = array(
			array('email'),
			array('encrypted_password'),
			//array('name', 'message' => 'nama tidak boleh kosong!'),
			//array('username', 'message' => 'username tidak boleh kosong!'),
			);

		static $before_destroy = array(
			'delete_uploaded_image'
			);

		public function image_url(){
			if($this->image_url){
				return base_url('uploads/user/'.$this->id.'/'.$this->image_url);
			}else{
				return false;
			}
		}

		public function upload_image()
		{
			$CI =& get_instance();
			$CI->load->library('upload');
			if(isset($_FILES['user_image']['size']) && $_FILES['user_image']['size']>0){
				$target_dir = getcwd().'/uploads/user/'.$this->id;
				echo $target_dir;
				if(!file_exists($target_dir)){
					mkdir($target_dir);
				}

				$config['upload_path'] = $target_dir;
				$config['allowed_types'] = 'gif|png|jpg|bmp';
				$config['max_size'] = '1000000';
				$config['max_width'] = '2048';
				$config['max_height'] = '1536';
				$CI->upload->initialize($config);

				$this->delete_uploaded_image();
				echo "Sampe";
				if($CI->upload->do_upload('user_image')){
					$upload_data = $CI->upload->data();
					$this->image_url = $upload_data['file_name'];
					return true;
				}
				else{
					echo $this->upload->display_errors();
					return false;
				}
			}
		}

		public function delete_uploaded_image(){
			$full_path = getcwd().'/uploads/user/'.$this->id.'/'.$this->image_url;
			if(isset($this->image_url) && file_exists($full_path)){
				unlink($full_path);
			}
		}

	}
 ?>