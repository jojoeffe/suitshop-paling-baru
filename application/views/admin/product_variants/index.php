
	<div class="container">
		<div class="product-breadcrumb">
			<ul class="breadcrumb">
				<li><a href="#">Dashboard</a> <span class="divider">/</span></li>
				<li class="active">Product Variant List</li>
			</ul>
		</div>
		<div class="row">
			<div class="span12">
				<a href="<?php echo site_url('admin/product_variant/add/').'/'.$pid; ?>" class="btn btn-success pull-right">Add Item</a>
			</div>
		</div>
		<hr>
		<div class="product-content">
			<table class="table">
				<tr>
					<th>#</th>
					<th>Color</th>
					<th>Images</th>
					<th>Size</th>
					<th>Price</th>
					<th>Stock</th>
					<th>Created at</th>
					<th>Updated at</th>
					<th>Action</th>
				</tr>
				<?php foreach ($productvar as $pv): ?>

				<tr>
					<td><?php echo $pv->id; ?></td>
					<td><?php echo $pv->color;?></td>
					<td><img src="<?php echo $pv->image_url(); ?>" width="200" alt="picture not available" /></td>
					<td><?php echo $pv->size; ?></td>
					<td><?php echo $pv->price ?></td>
					<td><?php echo $pv->stock ?></td>
					<td><?php echo $pv->created_at ?></td>
					<td><?php echo $pv->updated_at ?></td>
					<td><a href="<?php echo site_url('admin/product_variant/edit/'.$pv->id); ?>"><button class="btn btn-mini">edit</button></a>
						<a href="<?php echo site_url('admin/product_variant/destroy/'.$pid.'/'.$pv->id); ?>"><button class="btn btn-mini btn-danger">delete</button></a></td>
				</tr>
				<?php endforeach; ?>
			</table>
		</div>
		<div class="container">
			<!--
			<nav class="pagination">
				<ul>
					<li><a href="">Prev</a></li>
					<li><a href="">1</a></li>
					<li class="active"><a href="">2</a></li>
					<li><a href="">3</a></li>
					<li><a href="">4</a></li>
					<li><a href="">5</a></li>
					<li><a href="">Next</a></li>
				</ul>
			</nav>-->
			<?php echo admin_pagination(new Product) ?>
		</div>
