<div class="container">
<div class="product-breadcrumb">
	<ul class="breadcrumb">
		<li><a href="#">Dashboard</a> <span class="divider">/</span></li>
		<li>Product<span class="divider">/</span></li>
		<li><?php echo ucfirst($this->uri->segment(3)); ?></li>
	</ul>
</div>

<h2><?php echo $form_title ?></h2>
		
<div class="product-content">
	<div class="container">	
	<form class="form-horizontal" method="POST" action="<?php echo $form_action ?>" enctype="multipart/form-data">
		<div class="control-group">
			<label class="control-label" for="inputPrice">Color</label>
			<div class="controls">
				<input name="productvar[color]" value="<?php echo $productvar->color ?>" type="text" id="inputPrice" placeholder="">
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" for="inputPrice">Size</label>
			<div class="controls"><input name="productvar[size]" value="<?php echo $productvar->size ?>" type="text" id="inputPrice" placeholder="">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="inputPrice">Price</label>
			<div class="controls"><input name="productvar[price]" value="<?php echo $productvar->price ?>" type="text" id="inputPrice" placeholder="">
			</div>
		</div>		

		<div class="control-group">
			<label class="control-label" for="inputStock">Stock</label>
			<div class="controls"><input name="productvar[stock]" value="<?php echo $productvar->stock ?>" type="text" id="inputStock" placeholder=""></div>
		</div>	
		
		<div class="control-group">
			<label class="control-label" for="inputStock">Image	</label>
			<?php if($productvar->image_url()): ?>
			<img class="img-polaroid" src="<?php echo $productvar->image_url() ?>"  width="200" />
			<?php endif; ?>
			<div class="controls">
				<input name="product_image" type="file" placeholder="">
			</div>
		</div>	
		
		<div class="control-group pull-right">
			<div class="controls">
				<button class="btn" type="reset">Cancel</button>
				<button class="btn btn-success" type="submit" ><?php echo $form_title; ?></button>
			</div>
		</div>
	</form>
	</div>
</div>
</div>