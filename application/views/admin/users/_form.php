<div class="container">
<div class="product-breadcrumb">
	<ul class="breadcrumb">
		<li><a href="#">Dashboard</a> <span class="divider">/</span></li>
		<li>Users<span class="divider">/</span></li>
		<li><?php echo ucfirst($this->uri->segment(3)); ?></li>
	</ul>
</div>

<h2><?php echo $form_title ?></h2>
		
<div class="product-content">
	<div class="container">	
	<form class="form-horizontal" method="POST" action="<?php echo $form_action ?>" enctype="multipart/form-data">
		<div class="control-group">
			<label class="control-label">Username</label>
			<div class="controls"><input name="user[username]" value="<?php echo $user->username ?>" type="text" placeholder=""></div>
		</div>
		<div class="control-group">
			<label class="control-label" for="inputStock">Password</label>
			<div class="controls"><input name="user[password]" value="<?php echo $user->password ?>" type="password" placeholder=""></div>
		</div>
		<br>
		<div class="control-group">
			<label class="control-label">Name</label>
			<div class="controls"><input name="user[name]" value="<?php echo $user->name ?>" type="text" placeholder=""></div>
		</div>
		<div class="control-group">
			<label class="control-label">Email</label>
			<div class="controls"><input name="user[email]" value="<?php echo $user->email ?>" type="text" placeholder=""></div>
		</div>
		<div class="control-group">
			<label class="control-label">Phone</label>
			<div class="controls"><input name="user[phone]" value="<?php echo $user->phone ?>" type="text" placeholder=""></div>
		</div>
		<div class="control-group">
			<label class="control-label">Profile Picture</label>
			<?php if($user->image_url()): ?>
			<img class="img-polaroid" src="<?php echo $user->image_url() ?>"  width="200" />
			<?php endif; ?>
			<div class="controls"><input name="user_image" type="file"  placeholder=""></div>
		</div>
		
			

		<div class="control-group pull-right">
			<div class="controls">
				<button class="btn" type="reset">Cancel</button>
				<button class="btn btn-success" type="submit" ><?php echo $form_title ?></button>
			</div>
		</div>
	</form>
	</div>
</div>
</div>