
<div class="container">
<div class="product-breadcrumb">
			<ul class="breadcrumb">
				<li><a href="#">Dashboard</a> <span class="divider">/</span></li>
				<li class="active">Users</li>
			</ul>
		</div>
		<div class="row">
			<div class="span12">
				<a href="<?php echo site_url('admin/users/add') ?>"><button class="btn btn-success pull-right">Add User</button></a>
			</div>
		</div>

<div class="container">
		<div class="index-subtitle">
			<h3>Last Updated Items</h3>
			<table class="table table-bordered table-striped">
				<tr>
					<th>#</th>
					<th>Username</th>
					<th>Profile Picture</th>
					<th>Name</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Created_at</th>
					<th>Updated_at</th>
					<th>Actions</th>
				</tr>
				<?php foreach($users as $user): ?>
					<tr>
						<td><?php echo $user->id; ?></td>
						<td><?php echo $user->username; ?></td>
						<td><?php if($user->image_url()): ?>
						<img src="<?php echo $user->image_url() ?>"  width="500" />
						<?php endif; ?></td>
						<td><?php echo $user->name; ?></td>
						<td><?php echo $user->email; ?></td>
						<td><?php echo $user->phone; ?></td>
						<td><?php echo $user->created_at; ?></td>
						<td><?php echo $user->updated_at; ?></td>
						<td><a class='btn' href="<?php echo site_url('admin/users/edit').'/'.$user->id ?>">edit</a><a class='btn' href="<?php echo site_url('admin/users/destroy').'/'.$user->id ?>">delete</a></td>
					</tr>
				<?php endforeach; ?>
			</table>
		</div>
	</div>
</div>