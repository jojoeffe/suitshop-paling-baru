<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	
	<link rel="stylesheet" href="<?php echo base_url('assets/css').'/normalize.min.css'; ?> ">
	<link rel="stylesheet" href="<?php echo base_url('assets/css').'/bootstrap.css'; ?>" >
	<link rel="stylesheet" href="<?php echo base_url('assets/css').'/admin.css'; ?>">

	<!--[if lt IE 9]>
		<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<script src="js/HTML5Shiv.js"></script>
	<![endif]-->
	</head>
	<body>
		<header class="main-header">
			<div class="container">
				<div class="navbar">
					<div class="navbar-inner">
						<a class="brand" href="#">Suit Shop CMS</a>
						<ul class="nav pull-right">
							<li class="active"><a href="<?php echo site_url('admin/products/') ?>">Product List</a></li>
							<li><a href="#">Content Management</a></li>
							<li><a href="<?php echo site_url('admin/users'); ?>">Users</a></li>
							<li class="dropdown">
								<a href="#" role="button" data-target="#" class="dropdown-toggle" data-toggle="dropdown">
								Hello, Admin!
								<b class="caret"></b>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#"><i class="icon-user icon-black"></i> Profile</a></li>
									<li><a href="#"><i class="icon-cog icon-black"></i> Change Password</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-lock icon-black"></i> Log Out</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</header>

		<?php $this->load->view($body); ?>

		<footer>
			<div class="container">
				<hr>
				&copy; 2013 - Suit Shop
			</div>
		</footer>

		<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>-->
		<script>window.jQuery || document.write('<script src="js/jquery-1.10.1.min.js"><\/script>')</script>
		<script src="<?php echo base_url('assets/js/').'/bootstrap.min.js';?>"></script>
		<script src="<?php echo base_url('assets/js/').'/admin.js';?>"></script>
	</body>
</html>