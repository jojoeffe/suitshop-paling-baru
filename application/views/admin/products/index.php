
	<div class="container">
		<div class="product-breadcrumb">
			<ul class="breadcrumb">
				<li><a href="#">Dashboard</a> <span class="divider">/</span></li>
				<li class="active">Product List</li>
			</ul>
		</div>
		<div class="row">
			<div class="span12">
				<a href="<?php echo site_url('admin/products/add/')?>" class="btn btn-success pull-right">Add Item</a>
			</div>
		</div>
		<hr>
		<div class="product-content">
			<table class="table">
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Images</th>
					<th>Description</th>
					<th>Created at</th>
					<th>Updated at</th>
					<th>Action</th>
				</tr>

				<?php foreach ($products as $product): ?>

				<tr>
					<td><?php echo $product->id ?></td>
					<td><?php echo $product->name; if($product->is_featured) echo "<div><span class='label-success label'>featured</span></div>"; ?></td>
					<td><img src="<?php
						$selected = ProductVariant::find_by_sql('select image_url, id from product_variants where product_id='.$product->id.' limit 0,1;');
						if(!empty($selected)){
							echo base_url('uploads/product/').'/'.	$selected['0']->id.'/'.$selected['0']->image_url;
						}
						?>" width="200"/>
					</td>
					<td><?php echo $product->description; ?></td>
					<td><?php echo $product->created_at ?></td>
					<td><?php echo $product->updated_at ?></td>
					<td><a href="<?php echo site_url('admin/products/edit/'.$product->id); ?>"><button class="btn btn-mini">edit</button></a>
						<a href="<?php echo site_url('admin/products/destroy/'.$product->id); ?>"><button class="btn btn-mini btn-danger">delete</button></a>
						<a href="<?php echo site_url('admin/product_variant/index/'.$product->id); ?>"><button class="btn btn-mini btn-success">Product Variant</button></a>
					</td>	
				</tr>
				<?php endforeach; ?>
			</table>
		</div>
		<div class="container">
			<!--
			<nav class="pagination">
				<ul>
					<li><a href="">Prev</a></li>
					<li><a href="">1</a></li>
					<li class="active"><a href="">2</a></li>
					<li><a href="">3</a></li>
					<li><a href="">4</a></li>
					<li><a href="">5</a></li>
					<li><a href="">Next</a></li>
				</ul>
			</nav>-->
			<?php echo admin_pagination(new Product) ?>
		</div>
