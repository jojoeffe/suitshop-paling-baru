<div class="container">
<div class="product-breadcrumb">
	<ul class="breadcrumb">
		<li><a href="#">Dashboard</a> <span class="divider">/</span></li>
		<li>Product<span class="divider">/</span></li>
		<li><?php echo ucfirst($this->uri->segment(3)); ?></li>
	</ul>
</div>

<h2><?php echo $form_title ?></h2>
		
<div class="product-content">
	<div class="container">	
	<form class="form-horizontal" method="POST" action="<?php echo $form_action ?>" enctype="multipart/form-data">
		<div class="control-group">
			<label class="control-label" for="inputPrice">Name</label>
			<div class="controls">
				<input name="product[name]" value="<?php echo $product->name ?>" type="text" id="inputPrice" placeholder="">
				<div class='text-error'><?php if($product->errors) echo $product->errors->on('name'); ?></div>
			</div>
		</div>
		<!--
		<div class="control-group">
			<label class="control-label" for="inputStock">Category ID</label>
			<div class="controls"><input name="product[category_id]" value="<?php echo $product->category_id ?>" type="text" id="inputStock" placeholder=""></div>
		</div>-->


		<div class="control-group">
			<label class="control-label" for="inputStock">Category ID</label>
			<div class="controls">
				<?php foreach($leaves as $leaf): ?>
				<input name="product[category_id]" value="<?php echo $leaf ?>" type="radio" id="inputStock" placeholder=""> 
				<span style="margin-right:15px;">
					<?php
						$selected = Category::find($leaf);
						echo $selected->name;
					?>
				</span>
				<?php endforeach; ?>
			</div>
		</div>
	
		<div class="control-group">
			<label class="control-label" for="inputProductName">Description</label>
			<div class="controls"><textarea class="input-block-level" rows="10" id="inputProductName" name="product[description]" value="<?php echo $product->description ?>"></textarea></div>
		</div>
	
		<div class="control-group">
	        <label class="control-label" >Featured?</label>
	        <div class="controls">
	             <input name="product[is_featured]" class="input-large" type="checkbox" value="1" <?php if($product->is_featured) echo 'checked=checked'; ?> >
	        </div>
	    </div>

		<div class="control-group pull-right">
			<div class="controls">
				<button class="btn" type="reset">Cancel</button>
				<button class="btn btn-success" type="submit" ><?php echo $form_title; ?></button>
			</div>
		</div>
	</form>
	</div>
</div>
</div>