<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	
	<link rel="stylesheet" href="<?php echo base_url('assets/css').'/normalize.min.css'; ?> ">
	<link rel="stylesheet" href="<?php echo base_url('assets/css').'/bootstrap.css'; ?>" >
	<link rel="stylesheet" href="<?php echo base_url('assets/css').'/style.css'; ?>">

	<!--[if lt IE 9]>
		<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<script src="js/HTML5Shiv.js"></script>
	<![endif]-->
</head>
<body>
	<nav class="user-nav">
		<div class="container">
			<ul>
				<li>Welcome Guest!</li>
				<li><a href="#loginModal" data-toggle="modal" data-target="#loginModal">Login</a></li>
				<li><a href="#signUpModal" data-toggle="modal" data-target="#signUpModal">Sign Up</a></li>
				<li><a href="<?php echo site_url("/cart") ?>">  <i class="icon-shopping-cart icon-white"></i> Cart  </a> </li>
			</ul>
		</div>
	</nav>
	
	<header>
		<div class="container">
			<div class="row-fluid">
				<div class="span4">
					<img src="<?php echo base_url('assets/img').'/logo.png'; ?>" alt="Logo">
				</div>
				<div class="span8">
					<nav class="main-nav">
						<ul>
							<li><a href="<?php echo site_url('/home'); ?>">Home</a></li>
							<?php foreach($categories as $c): ?>
							<li><a href="<?php echo site_url('/products/category/').'/'.$c->id; ?>"><?php echo $c->name ?></a></li>
							<?php endforeach; ?>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</header>

	<?php $this->load->view($body); ?>
 
	<!-- Modal -->
	<div id="loginModal" class="modal hide fade"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h3 id="myModalLabel">Sign in</h3>
	  </div>
	  <div class="modal-body">
	    <form class="form-horizontal" method="POST"  action="<?php echo site_url('registrations/login') ?>">
	  <div class="control-group">
	    <label class="control-label" for="inputEmail">Email</label>
	    <div class="controls">
	      <input name="user[email]" type="text" id="inputEmail" placeholder="Email">
	    </div>
	  </div>
	  <div class="control-group">
	    <label class="control-label" for="inputPassword">Password</label>
	    <div class="controls">
	      <input name="user[password]" type="password" id="inputPassword" placeholder="Password">
	    </div>
	  </div>
	  <div class="control-group">
	    <div class="controls">
	    	<input type="hidden" name="ch" value="login">
	      <button type="submit" class="btn-primary btn">Sign in</button>
	    </div>
	  </div>
	</form>
	  </div>
	</div>

	<div id="signUpModal" class="modal hide fade"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h3 id="myModalLabel">Sign Up</h3>
	  </div>
	  <div class="modal-body">
	    <form class="form-horizontal" method="POST" action="<?php echo site_url('registrations/sign_up') ?>">
	  <div class="control-group">
	    <label class="control-label" for="inputEmail">Email</label>
	    <div class="controls">
	      <input name="user[email]" type="text" id="inputEmail" placeholder="Email">
	    </div>
	  </div>
	  <div class="control-group">
	    <label class="control-label" for="inputPassword">Password</label>
	    <div class="controls">
	      <input name="user[password]" type="password" id="inputPassword" placeholder="Password">
	    </div>
	  </div>	
	  <div class="control-group">
	    <label class="control-label" for="inputPassword">Confirm Password</label>
	    <div class="controls">
	      <input name="user[password_confirmation]" type="password" id="inputPassword" placeholder="Confirm Password">
	    </div>
	  </div>
	  <div class="control-group">
	    <div class="controls">
	      <button type="submit" class="btn btn-primary">Sign up</button>
	    </div>
	  </div>
	</form>
	  </div>
	</div>



	<footer>
		<div class="container">
			<div class="span12 footerrow">
				<a href="#" class="social">
					<img src="img/ico-twitter.png">                 
					<span> @SUIT_SHOP</span>
				</a>
				<a href="#" class="social">
					<img src="img/ico-facebook.png">
					<span> FACEBOOK </span>
				</a>
			</div>
		
			<div class="span12 footerrow">
				<b>ABOUT &middot PRIVACY POLICY &middot CONTACT</b>
			</div>
			<div class="span12 footerrow">
				&copy2013 SUITSHOP
			</div>
		</div>
	</footer>

	<!--  load jquery dari google  -->
	<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>-->

	<!--  Antisipasi jquery tidak ke load  -->
	<script>window.jQuery || document.write('<script src="<?php echo base_url('assets/js/').'/jquery-1.10.1.min.js';?>"><\/script>')</script>

	<script src="<?php echo base_url('assets/js/').'/bootstrap.min.js';?>"></script>
	<script src="<?php echo base_url('assets/js/').'/jquery.elevateZoom-2.5.5.min.js';?>"></script>
	<script src="<?php echo base_url('assets/js/').'/modernizr.js';?>"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="<?php echo base_url('assets/js/').'/gmaps.js';?>"></script>
	<script src="<?php echo base_url('assets/js/').'/main.js';?>"></script>
</body>
</html>