<div class="content">
		<div class="container">
			<div class="title">
				<h2 class="productTitle"><?php echo $product->name; ?></h2>
				
				
				<hr>
				<div class="row">
				<div class="span4">
					<img src="<?php
						$selected = ProductVariant::find_by_sql('select image_url, id from product_variants where product_id='.$product->id.' limit 0,1;');
						if(!empty($selected)){
							echo base_url('uploads/product/').'/'.	$selected['0']->id.'/'.$selected['0']->image_url;
						}
						?>" width="300" class="img-polaroid" />
				</div>
				<div class="span6">
						<div class="hargaProduk">
							<?php echo $productvar['0']->price; ?>
						</div>
						<div class="detailProduk">
							<?php echo $product->description; ?>
						</div>
						<form action="<?php echo site_url('/cart/add') ?>" method="POST">
						<div class="sizeList">
							Ukuran: <br>
							<select name="size">
								<?php foreach($productvarsize as $pv): ?>     
									<option value="<?php echo $pv->size; ?>"><?php echo $pv->size; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="colorList">
							Warna: <br>
							<select name="color">
								<?php foreach($productvarcolor as $pv): ?>
								<option value="<?php echo $pv->color; ?>"><?php echo $pv->color; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div>
							<input type="text" name="product_id" value="<?php echo $product->id; ?>">
						</div>
						<input class="btn btn-large btn-primary" value="Add to Cart" type="submit" />
						</form>
					</div>
				</div>
				<div class="recommendation">
					<h2>Recommendation</h2>
				</div>
				<div class="produk">
				<?php foreach($featured_products as $p): ?>
					<div class="span3">
						<a href="<?php echo site_url('products/detail/').'/'.$p->id; ?>">
							<img src="<?php echo $p->image_url() ?>" class="productImg"/>
							<span class="harga"><?php echo $p->price ?></span>
							<p><?php echo $p->product_id ?></p>
						</a>
					</div>
				<?php endforeach; ?>
			</div>
			</div>
			
		</div>
	</div>