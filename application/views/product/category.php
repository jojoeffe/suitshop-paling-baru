<div class="container">
	<div class="title">
		<h2 class="productTitle"><?php echo $title ?></h2>
		<hr id="garis">
	</div>
	
	<div class="produk">
		<?php foreach($products as $p): ?>
			<div class="span3">
				<a href="<?php echo site_url('products/detail/').'/'.$p->id; ?>">
					<img src="<?php echo $p->image_url() ?>" class="productImg" alt="picture not found"/>
					<span class="harga"><?php echo $p->price ?></span>
					<p><?php echo $p->product_id ?></p>
				</a>
			</div>
		<?php endforeach; ?>
	</div>
	
</div>