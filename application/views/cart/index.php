<?php

	if(!isset($_SESSION['login_user'])){
		//header('Location:'.site_url('home#loginModal').'?pagename='.basename($_SERVER['PHP_SELF'], ".php"));
	}
?>

<div class="container">
	<h1>Cart</h1>
	<hr>

	<table class="table">
		<tr>
			<th>id</th>
			<th>image</th>
			<th>product name</th>
			<th>size</th>
			<th>color</th>
			<th>price</th>
			<th>quantity</th>
		</tr>

		<?php foreach($items as $item): ?>
			<tr>
				<td><?php echo $item->product_id; ?></td>
				<td><image class="img" src="<?php echo base_url('uploads/product/').'/'.$item->id.'/'.$item->image_url; ?>" width="200"></image></td>
				<td>
					<?php 
						$id = $item->product_id;
						$selected = Product::find('all', array('conditions' => array('id = ?', $id)));
					 ?>
				</td>
				<td><?php echo $item->size; ?></td>
				<td><?php echo $item->color; ?></td>
				<td><?php echo $item->price; ?></td>
				<td>1</td>
			</tr>
		<?php endforeach; ?>
	</table>

</div>