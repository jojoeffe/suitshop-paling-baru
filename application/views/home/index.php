<div class="content">
		<div class="container">
			<div class="title">
				<h2 class="productTitle">Produk Pilihan</h2>
				<hr id="garis">
			</div>
			
			<div class="produk">
				<?php foreach($featured_products as $p): ?>
					<div class="span3">
						<a href="<?php echo site_url('products/detail/').'/'.$p->id; ?>">
							<img src="<?php echo $p->image_url() ?>" class="productImg"/>
							<span class="harga"><?php echo $p->price ?></span>
							<p><?php echo $p->product_id ?></p>
						</a>
					</div>
				<?php endforeach; ?>
			</div>
			
		</div>


		<div class="content">
		<div class="container">
			<div class="title">
				<h2 class="productTitle">Produk Terbaru</h2>
			</div>
			<div class="produk">
				<?php foreach($latest_products as $p): ?>
					<div class="span3">
						<a href="<?php echo site_url('products/detail/').'/'.$p->id; ?>">
							<img src="<?php echo $p->image_url(); ?>" class="productImg" /></img>
							<span class="harga"><?php echo $p->price ?></span>
							<p><?php echo $p->product_id ?></p>
						</a>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>