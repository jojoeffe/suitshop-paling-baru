<?php 
	class Products extends CI_Controller{
		function __construct(){
			parent::__construct();
		}

		function index($page=1){
			$this->data['products'] = Product::page($page);
			$this->data['body'] = 'admin/products/index';
			$this->load->view('admin/layouts/admin', $this->data);
		}

		//hanya menampilkan form
		function add(){
			$cat = Category::find('all');
			$jml = count($cat);
			$idd;
			$p_idd;

			for($ii=0;$ii<$jml;$ii++){
				$idd[$ii] = $cat["".$ii]->id;
				$p_idd[$ii] = $cat["".$ii]->parent_id;
			}

			$leaves=array();

			for($ii=0;$ii<$jml;$ii++){
				if(in_array($idd[$ii], $p_idd)){

				}else{
					array_push($leaves, $idd[$ii]);
				}
			}
			
			$this->data['leaves'] = $leaves;
			$this->data['product'] = new Product();
			$this->data['form_title'] = 'add product';
			$this->data['form_action'] = site_url('admin/products/create');
			$this->data['action']='Add';
			$this->data['body'] = 'admin/products/add';
			$this->load->view('admin/layouts/admin', $this->data);

		}

		//hanya menampilkan form
		function edit($id){
			$cat = Category::find('all');
			$jml = count($cat);
			$idd;
			$p_idd;

			for($ii=0;$ii<$jml;$ii++){
				$idd[$ii] = $cat["".$ii]->id;
				$p_idd[$ii] = $cat["".$ii]->parent_id;
			}

			$leaves=array();

			for($ii=0;$ii<$jml;$ii++){
				if(in_array($idd[$ii], $p_idd)){

				}else{
					array_push($leaves, $idd[$ii]);
				}
			}
			
			$this->data['leaves'] = $leaves;

			$this->data['product'] = Product::find($id);
			$this->data['form_title'] = 'Edit '.$this->data['product']->name;
			$this->data['form_action'] = site_url('admin/products/update/')."/".$this->data['product']->id;
			$this->data['body'] = 'admin/products/edit';
			$this->load->view('admin/layouts/admin', $this->data);
		}

		function create(){
			$this->data['product'] = new Product($this->input->post('product'));
			if($this->data['product']->save()){
				
				if($this->data['product']->upload_image()){
					$this->data['product']->save();
				}
				redirect(site_url('admin/products'));
			}else{
				$this->data['form_title'] = 'add product';
				$this->data['form_action'] = site_url('admin/products/create');
				$this->data['action']='Add';
				$this->data['body'] = 'admin/products/add';
				$this->load->view('admin/layouts/admin', $this->data);
			}
		}

		function update($id){
			$this->data['product'] = Product::find($id);
			$this->data['product']->update_attributes($this->input->post('product'));
			if($this->data['product']->save()){
				if($this->data['product']->upload_image()){
					$this->data['product']->save();
				}
				redirect(site_url('admin/products'));
			}else{
				$this->data['product'] = Product::find($id);
				$this->data['form_title'] = 'Edit '.$this->data['product']->name;
				$this->data['form_action'] = site_url('admin/products/update/')."/".$this->data['product']->id;
				$this->data['body'] = 'admin/products/edit';
				$this->load->view('admin/layouts/admin', $this->data);
			}
		}

		function destroy($id){
			$product = Product::find($id);
			
			$pv = ProductVariant::find('all');
			$tbd = Array();
			

			foreach($pv as $pvv){
				if(($pvv->product_id)==$id){
					array_push($tbd, $pvv->id);
				}
			}


			ProductVariant::table()->delete(array('id' => $tbd)); 
			$product->delete();

			redirect(site_url('admin/products'));
		}
	}

 ?>