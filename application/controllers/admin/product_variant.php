<?php 
	class product_variant extends CI_Controller{
		function __construct(){
			parent::__construct();
		}

		function index($pid){
			$selected = ProductVariant::find_by_sql('select * from product_variants where product_id='.$pid.';');
			if(!empty($selected)){
				$this->data['pid'] = $pid;
				$this->data['productvar'] = $selected;
				$this->data['body'] = 'admin/product_variants/index';
				$this->load->view('admin/layouts/admin', $this->data);
			}else{
				$this->data['productvar'] = new ProductVariant();
				$produk = Product::find($pid);
				$this->data['form_title'] = 'There are no variants for this product:'.$produk->name.', please add one';
				$this->data['form_action'] = site_url('admin/product_variant/create').'/'.$pid;
				$this->data['action']='Add';
				$this->data['body'] = 'admin/product_variants/add';
				$this->load->view('admin/layouts/admin', $this->data);
			}
		}

		//hanya menampilkan form
		function add($pid){
			$this->data['productvar'] = new ProductVariant();
			$produk = Product::find($pid);
			$this->data['form_title'] = 'Add Product Variant for Product: '.$produk->name;
			$this->data['form_action'] = site_url('admin/product_variant/create').'/'.$pid;
			$this->data['action']='Add';
			$this->data['body'] = 'admin/product_variants/add';
			$this->load->view('admin/layouts/admin', $this->data);

		}

		//hanya menampilkan form
		function edit($id){
			$this->data['productvar'] = ProductVariant::find($id);
			$this->data['form_title'] = 'Edit';
			$this->data['form_action'] = site_url('admin/product_variant/update/')."/".$this->data['productvar']->id;
			$this->data['body'] = 'admin/product_variants/edit';
			$this->load->view('admin/layouts/admin', $this->data);
		}

		function create($pid){
			$this->data['product'] = new ProductVariant($this->input->post('productvar'));
			//$temp = $this->data['product'];
			$this->data['product']->product_id = $pid;
			//print_r($temp);
			//echo "ini";
			if($this->data['product']->save()){
				echo "MASUK";
				//exit;
				if($this->data['product']->upload_image()){
					$this->data['product']->save();
				}

				redirect(site_url('admin/product_variant/index').'/'.$pid);
			}else{
				echo "masuk else";
				$this->data['form_title'] = 'add product';
				$this->data['form_action'] = site_url('admin/product_variants/create');
				$this->data['action']='Add';
				$this->data['body'] = 'admin/product_variants/add';
				$this->load->view('admin/layouts/admin', $this->data);
			}
		}

		function update($id, $pid){
			$this->data['product'] = ProductVariant::find($id);
			$this->data['product']->update_attributes($this->input->post('product'));
			if($this->data['product']->save()){
				if($this->data['product']->upload_image()){
					$this->data['product']->save();
				}
				redirect(site_url('admin/product_variant/index').'/'.$pid);
			}else{
				$this->data['product'] = ProductVariant::find($id);
				$this->data['form_title'] = 'Edit '.$this->data['product']->name;
				$this->data['form_action'] = site_url('admin/product_variants/update/')."/".$this->data['product']->id;
				$this->data['body'] = 'admin/product_variants/edit';
				$this->load->view('admin/layouts/admin', $this->data);
			}
		}

		function destroy($pid, $id){
			$product = ProductVariant::find($id);
			$product->delete();
			
			redirect(site_url('admin/product_variant/index').'/'.$pid);
		}
	}

 ?>