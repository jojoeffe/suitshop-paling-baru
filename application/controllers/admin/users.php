<?php 
	class Users extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->helper('url');
		}

		function index(){
			$this->data['users'] = User::find('all');
			$this->data['body'] = 'admin/users/index';
			$this->load->view('admin/layouts/admin', $this->data);
		}
		function sendMail()
	    {
	    	echo "aaaa";
	    	$this->load->library('email');
			$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'effendijohanes@gmail.com', // change it to yours
			'smtp_pass' => 'penaburml', // change it to yours
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
			);

			//$message = $this->load->view('upload_success','',TRUE);
			$message = "test isinya";
			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from('effendijohanes@gmail.com'); // change it to yours
			$this->email->to('johanes.effendi@live.com');// change it to yours
			$this->email->subject('Suit shop email!');
			$this->email->message($message);
			if($this->email->send())
			{
				echo 'Email sent.';
			}else{
				show_error($this->email->print_debugger());
			}

			$subject="Test mail";
			$to="johanes.effendi@live.com";
			$body="This is a test mail";
			if (mail($to,$subject,$body))
			echo "Mail sent successfully!";
			else
			echo"Mail not sent!";

		$headers = "MIME-Version: 1.0";
		$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
		$headers .= "From: effendijohanes@gmail.com" . "\r\n";
		mail("johanes.effendi@live.com","test subject","test body",$headers);

	    }

		function emaill(){
			$this->load->library('email');

			$this->email->from('your@example.com', 'Your Name');
			$this->email->to('effendijohanes@gmail.com'); 
			//$this->email->cc('another@another-example.com'); 
			//$this->email->bcc('them@their-example.com'); 

			$this->email->subject('Email Test');
			$this->email->message('Testing the email class.');	

			$this->email->send();

			echo $this->email->print_debugger();
		}

		//hanya menampilkan form
		function add(){
			$this->data['user'] = new User();
			$this->data['form_title'] = 'Add User';
			$this->data['form_action'] = site_url('admin/users/create');
			$this->data['action']='Add';
			$this->data['body'] = 'admin/users/add';
			$this->load->view('admin/layouts/admin', $this->data);

		}

		//hanya menampilkan form
		function edit($id){
			$this->data['user'] = User::find($id);
			$this->data['form_title'] = 'Edit '.$this->data['user']->name;
			$this->data['form_action'] = site_url('admin/users/update/')."/".$this->data['user']->id;
			$this->data['body'] = 'admin/users/edit';
			$this->load->view('admin/layouts/admin', $this->data);
		}

		function create(){

			$this->data['user'] = new User($this->input->post('user'));
			if($this->data['user']->save()){	
				if($this->data['user']->upload_image()){
					$this->data['user']->save();
				}
				redirect(site_url('admin/users'));	
			}else{
				$this->data['form_title'] = 'add user';
				$this->data['form_action'] = site_url('admin/users/create');
				$this->data['action']='Add';
				$this->data['body'] = 'admin/users/add';
				$this->load->view('admin/layouts/admin', $this->data);
			}
			
		}

		function update($id){
			$this->data['user'] = User::find($id);
			$this->data['user']->update_attributes($this->input->post('user'));
			if($this->data['user']->save()){
				if($this->data['user']->upload_image()){
					$this->data['user']->save();
				}
				redirect(site_url('admin/users'));
			}else{
				$this->data['user'] = User::find($id);
				$this->data['form_title'] = 'Edit '.$this->data['user']->name;
				$this->data['form_action'] = site_url('admin/users/update/')."/".$this->data['user']->id;
				$this->data['body'] = 'admin/users/edit';
				$this->load->view('admin/layouts/admin', $this->data);
			}
		}

		function destroy($id){
			$user = User::find($id);
			$user->delete();
			redirect(site_url('admin/users'));
		}
	}

 ?>