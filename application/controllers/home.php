<?php 
	class Home extends CI_Controller
	{
		function __construct(){
			parent::__construct();
		}

		public function index(){
			$this->data['products'] = Product::find('all', array('limit' => 4));

			$latest_p = Product::find('all', array('order' => 'created_at DESC', 'limit' => 4));
			
			$selected = Array();
			foreach ($latest_p as $lp) {
				$first = ProductVariant::find_by_sql('select * from product_variants where product_id='.$lp->id.' limit 0,1;');
				if(!empty($first)){
					array_push($selected, $first['0']);
				}
			}
			$this->data['latest_products']=$selected;



			$featured_p = Product::find('all', array('conditions' => 'is_featured = 1', 'limit' => 4, 'order' => 'created_at DESC'));
			$selected1 = Array();


			foreach ($featured_p as $fp) {
				$first = ProductVariant::find_by_sql('select * from product_variants where product_id='.$fp->id.' limit 0,1;');
				array_push($selected1, $first['0']);
			}
			$this->data['featured_products']=$selected1;


			$cat = Category::find('all');
			$jml = count($cat);
			$idd;
			$p_idd;

			for($ii=0;$ii<$jml;$ii++){
				$idd[$ii] = $cat["".$ii]->id;
				$p_idd[$ii] = $cat["".$ii]->parent_id;
			}

			$leaves=array();

			for($ii=0;$ii<$jml;$ii++){
				if(in_array($idd[$ii], $p_idd)){

				}else{
					array_push($leaves, $idd[$ii]);
				}
			}

			$this->data['categories'] = Category::find('all', array('conditions' => array('id in (?)', $leaves)));
			//$this->data['categories'] = $leaves;
 				
			$this->data['body'] = 'home/index';
			$this->load->vars($this->data);
			$this->load->view('layouts/application');
		}
	}

 ?>