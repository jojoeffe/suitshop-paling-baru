<?php 
	class Cart extends CI_Controller{

		function index(){
			$cat = Category::find('all');
			$jml = count($cat);
			$idd;
			$p_idd;

			for($ii=0;$ii<$jml;$ii++){
				$idd[$ii] = $cat["".$ii]->id;
				$p_idd[$ii] = $cat["".$ii]->parent_id;
			}

			$leaves=array();

			for($ii=0;$ii<$jml;$ii++){
				if(in_array($idd[$ii], $p_idd)){

				}else{
					array_push($leaves, $idd[$ii]);
				}
			}

			$this->data['categories'] = Category::find('all', array('conditions' => array('id in (?)', $leaves)));
			
			$cart = CartModel::find('all', array('conditions' => array('user_id = ?', '1106053634')));
			
			if(empty($cart)){
				$cart = new CartModel();
				$cart->user_id = 1106053634;
				$cart->save();
			}
			$items = CartItemModel::find('all', array('conditions' => array('cart_id = ?', $cart['0']->id)));
			echo $cart['0']->id;
			$output=Array();
			foreach($items as $item){
				$pv = ProductVariant::find($item->product_detail_id);
				array_push($output, $pv);
			}

			$this->data['items'] = $output;
			$this->data['body'] = 'cart/index';
			$this->load->view('layouts/application', $this->data);
		}

		function add(){
			$size = $this->input->post('size');
			$color = $this->input->post('color');
			$product_id = $this->input->post('product_id');
			//$hasil = $this->input->post['productvar'];
			$selected = ProductVariant::find('all', array('conditions' => array('color = ? AND size= ? AND product_id= ?', $color, $size, $product_id)));
			

			//cek apakah cart dengan user jojoeffe(id 1106053634) sudah ada atau belum
			$cart = CartModel::find('all', array('conditions' => array('user_id = ?', 1106053634)));
			$cart=array();
			if(empty($cart)){
				$cart = new CartModel();
				$cart->user_id = 1106053634;
				$cart->save();
			}
			$newitem = new CartItemModel();
			//$newitem->cart_id = $cart->id;
			$newitem->cart_id = 1;
			$newitem->quantity = 1;
			$newitem->product_detail_id = $selected['0']->id;

			$newitem->save();
			redirect(site_url('cart'));
		}
	}
?>