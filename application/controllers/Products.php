<?php 
	class Products extends CI_Controller{
		public function index(){
			$this->data['body'] = 'product/category';
			$this->load->view('layout/application', $this->data);
		}

		public function category($id){
			//$prod = Product::find_by_sql('select * from product where category_id='.$id.';');
			$prod = Product::find('all', array('conditions' => array('category_id = ?', $id)));
			
			$selected = Array();
			foreach ($prod as $p) {
				$first = ProductVariant::find_by_sql('select * from product_variants where product_id='.$p->id.' limit 0,1;');
				if(!empty($first)){
					array_push($selected, $first['0']);
				}
			}
			$this->data['products']=$selected;


			$cat = Category::find('all');
			$jml = count($cat);
			$idd;
			$p_idd;

			for($ii=0;$ii<$jml;$ii++){
				$idd[$ii] = $cat["".$ii]->id;
				$p_idd[$ii] = $cat["".$ii]->parent_id;
			}

			$leaves=array();

			for($ii=0;$ii<$jml;$ii++){
				if(in_array($idd[$ii], $p_idd)){

				}else{
					array_push($leaves, $idd[$ii]);
				}
			}

			$haha = Category::find($id);

			$this->data['title'] = $haha->name;
			$this->data['categories'] = Category::find('all', array('conditions' => array('id in (?)', $leaves)));
			

			$this->data['body'] = 'product/category';
			$this->load->view('layouts/application', $this->data);
		}

		public function detail($pvid){
			$prodvar = ProductVariant::find($pvid);
			$prodid = $prodvar->product_id;
			$prod = Product::find($prodid);
			$prodvar1 = ProductVariant::find_by_sql('select * from product_variants where product_id='.$prodid.';');
			$prodvarsize = ProductVariant::find_by_sql('select distinct size from product_variants where product_id='.$prodid.';');
			$prodvarcolor = ProductVariant::find_by_sql('select distinct color from product_variants where product_id='.$prodid.';');


			$featured_p = Product::find('all', array('conditions' => 'is_featured = 1', 'limit' => 4, 'order' => 'created_at DESC'));
			$selected1 = Array();


			foreach ($featured_p as $fp) {
				$first = ProductVariant::find_by_sql('select * from product_variants where product_id='.$fp->id.' limit 0,1;');
				array_push($selected1, $first['0']);
			}
			$this->data['featured_products']=$selected1;

			$this->data['product'] = $prod;
			$this->data['productvar'] = $prodvar1;
			$this->data['productvarsize'] = $prodvarsize;
			$this->data['productvarcolor'] = $prodvarcolor;
			
			$cat = Category::find('all');
			$jml = count($cat);
			$idd;
			$p_idd;

			for($ii=0;$ii<$jml;$ii++){
				$idd[$ii] = $cat["".$ii]->id;
				$p_idd[$ii] = $cat["".$ii]->parent_id;
			}

			$leaves=array();

			for($ii=0;$ii<$jml;$ii++){
				if(in_array($idd[$ii], $p_idd)){

				}else{
					array_push($leaves, $idd[$ii]);
				}
			}

			$this->data['categories'] = Category::find('all', array('conditions' => array('id in (?)', $leaves)));
			


			$this->data['body'] = 'product/detail';
			$this->load->view('layouts/application', $this->data);
		}
	}


 ?>