var Site = {

	init: function(){
		$('html').removeClass('no-js');

		if($('#zoom').length)
			Site.zoom();

		if($('#map').length)
			Site.map();

	},

	zoom: function(){
		$('#zoom').elevateZoom({
			zoomWindowWidth: 300,
			zoomWindowHeight: 300,
			easing: true
		});
	},	

	map: function(){
		var map = new GMaps({
					  div: '#map',
					  lat: -6.368941,
					  lng: 106.831002
					}); 	
		map.addMarker({
					  lat: -6.368941,
					  lng: 106.831002,
					  title: 'Main Office'
					});
	}
};

$(function (){
	Site.init();
});